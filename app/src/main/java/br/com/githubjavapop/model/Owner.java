package br.com.githubjavapop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Owner {

    @SerializedName("login")
    @Expose
    private String login;

    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;

    @SerializedName("id")
    @Expose
    private String id;



    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String photo) {
        this.avatarUrl = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

