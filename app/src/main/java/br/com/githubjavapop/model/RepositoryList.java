package br.com.githubjavapop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RepositoryList {

    @SerializedName("items")
    @Expose
    private List<Repository> repos;


    public List<Repository> getRepos() {
        return repos;
    }

}
