package br.com.githubjavapop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PullRequest {

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("title")
    @Expose
    private String pullRequestName;

    @SerializedName("body")
    @Expose
    private String pullRequestDescription;

    @SerializedName("html_url")
    @Expose
    private String pullRequestUrl;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("created_at")
    @Expose
    private String dateCreated;

    public User getUser() {
        return user;
    }

    public String getPullRequestName() {
        return pullRequestName;
    }

    public String getPullRequestDescription() {
        return pullRequestDescription;
    }

    public String getPullRequestUrl() {
        return pullRequestUrl;
    }

    public String getId() {
        return id;
    }

    public String getDateCreated() {
        return dateCreated;
    }
}
