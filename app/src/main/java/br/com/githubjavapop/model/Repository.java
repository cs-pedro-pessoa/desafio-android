package br.com.githubjavapop.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static android.R.attr.id;

public class Repository implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("owner")
    @Expose
    private Owner owner;

    @SerializedName("forks_count")
    @Expose
    private String forksCount;

    @SerializedName("stargazers_count")
    @Expose
    private String starsCount;

    @SerializedName("full_name")
    @Expose
    private String fullName;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("open_issues_count")
    @Expose
    private String numberOpenIssues;


    protected Repository(Parcel in) {
        name = in.readString();
        description = in.readString();
        forksCount = in.readString();
        starsCount = in.readString();
        fullName = in.readString();
        id = in.readString();
        numberOpenIssues = in.readString();
    }

    public Repository() {

    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Owner getOwner() {
        return owner;
    }

    public String getForksCount() {
        return forksCount;
    }

    public String getStarsCount() {
        return starsCount;
    }

    public String getFullName() {
        return fullName;
    }

    public String getId() {
        return id;
    }

    public String getNumberOpenIssues() {
        return numberOpenIssues;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(forksCount);
        dest.writeString(starsCount);
        dest.writeString(fullName);
        dest.writeString(id);
        dest.writeString(numberOpenIssues);
    }
}