package br.com.githubjavapop.api;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@EBean(scope = EBean.Scope.Singleton)
public class GitHubService {

    private static RetrofitAPI retrofitAPI;

    @AfterInject
    public void makeRetrofitCalls(){
        if (retrofitAPI != null) {
            return;
        }

        String baseUrl = "https://api.github.com";
        retrofitAPI = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(RetrofitAPI.class);
    }

    public RetrofitAPI getRetrofitAPI() {
        return retrofitAPI;
    }
}
