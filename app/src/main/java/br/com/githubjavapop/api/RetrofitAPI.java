package br.com.githubjavapop.api;

import java.util.List;

import br.com.githubjavapop.model.PullRequest;
import br.com.githubjavapop.model.RepositoryList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitAPI {

    @GET("search/repositories")
    Call<RepositoryList> retrieveJavaRepositories(
            @Query("q") String language,
            @Query("sort") String stars,
            @Query("page") int page
    );

    @GET("repos/{creator}/{repository}/pulls")
    Call<List<PullRequest>> retrievePullRequests(
            @Path("creator") String creator,
            @Path("repository") String repository
    );
}
