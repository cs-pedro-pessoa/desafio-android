package br.com.githubjavapop.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import br.com.githubjavapop.R;
import br.com.githubjavapop.adapter.PullRequestAdapter;
import br.com.githubjavapop.adapter.RepositoryAdapter;
import br.com.githubjavapop.api.RetrofitAPI;
import br.com.githubjavapop.listener.EndlessRecyclerViewScrollListener;
import br.com.githubjavapop.model.PullRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PullRequestActivity extends AppCompatActivity {

    @BindView(R.id.pullrequest_recycler_view)
    RecyclerView pullRequestRecyclerView;

    private LinearLayoutManager pullRequestLinearLayoutManager;

    private PullRequestAdapter pullRequestAdapter;

    private List<PullRequest> pullRequests;

    private RetrofitAPI pullRequestAPI;

    private int actualPage = 0;

    private String repoOwner;

    private String repoName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pullrequest_activity);

        repoOwner = getIntent().getStringExtra(RepositoryAdapter.REPO_OWNER);
        repoName = getIntent().getStringExtra(RepositoryAdapter.REPO_NAME);

        ButterKnife.bind(this);
        getSupportActionBar().setTitle(repoName);

        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.github_api_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        pullRequestAPI = retrofit.create(RetrofitAPI.class);

        pullRequestLinearLayoutManager = new LinearLayoutManager(this);

        pullRequestRecyclerView.setLayoutManager(pullRequestLinearLayoutManager);

        pullRequestAdapter = new PullRequestAdapter(new ArrayList<PullRequest>());

        pullRequestRecyclerView.setAdapter(pullRequestAdapter);

        makePullRequestCall(actualPage);

        pullRequestRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(pullRequestLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                makePullRequestCall(page);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        makePullRequestCall(actualPage);
    }

    private void makePullRequestCall(int page) {

        actualPage = page + 1;

        Call<List<PullRequest>> pullRequestCall;

        pullRequestCall = pullRequestAPI.retrievePullRequests(repoOwner, repoName);

        pullRequestCall.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                int responseCode = response.code();
                pullRequests = response.body();
                Log.d("Teste", "onResponse: " + responseCode);
                ((PullRequestAdapter) pullRequestRecyclerView.getAdapter()).addMorePullRequests(pullRequests);
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {

            }
        });
    }



}



