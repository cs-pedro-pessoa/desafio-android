package br.com.githubjavapop.activity;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import br.com.githubjavapop.R;
import br.com.githubjavapop.adapter.RepositoryAdapter;
import br.com.githubjavapop.api.RetrofitAPI;
import br.com.githubjavapop.listener.EndlessRecyclerViewScrollListener;
import br.com.githubjavapop.model.Repository;
import br.com.githubjavapop.model.RepositoryList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// TODO: Salvar estado (Strings language..., RepositoryAdapter, LinearLayout, )
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ProgressDialog progressDialog;

    private RepositoryAdapter repositoryAdapter;

    private ArrayList<Repository> repositories;

    private LinearLayoutManager linearLayoutManager;

    private static final String LIST_STATE_KEY = "recycler_view_state";

    private RetrofitAPI repositoryAPI;

    private String language;
    private String stars;
    private int actualPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null){

        }
        ButterKnife.bind(this);
        loadScreen();

        setupQueryParameters();

        // TODO: Objeto caro! Criar 1 vez com singleton!
        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.github_api_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        repositoryAPI = retrofit.create(RetrofitAPI.class);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        repositoryAdapter = new RepositoryAdapter(new ArrayList<Repository>());
        recyclerView.setAdapter(repositoryAdapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                makeRepositoryRequest(page);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // TODO: Refatorar chamada onResume
        makeRepositoryRequest(actualPage);
    }


    private void makeRepositoryRequest(int page) {
        actualPage = page + 1;
        Call<RepositoryList> repositoryCall;

        repositoryCall = repositoryAPI.retrieveJavaRepositories(language, stars, actualPage);

        repositoryCall.enqueue(new Callback<RepositoryList>() {
            @Override
            public void onResponse(Call<RepositoryList> call, Response<RepositoryList> response) {
                int responseCode = response.code();
                Log.d("Repository", "onResponse: " + responseCode);

                RepositoryList repositoryList = response.body();

                if (response.isSuccess()) {
                    progressDialog.hide();
                    ((RepositoryAdapter) recyclerView.getAdapter()).addMoreRepositories(repositoryList.getRepos());
                }
            }

            @Override
            public void onFailure(Call<RepositoryList> call, Throwable t) {
                Log.d("Repository", "onResponse: " + t.getMessage());
                retryConnectionDialog();
            }
        });
    }

    private void loadScreen() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.load_retrofit_request_dialog));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void setupQueryParameters() {
        language = getResources().getString(R.string.language_java);
        stars = getResources().getString(R.string.sort_stars);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(LIST_STATE_KEY, repositories);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            repositories = savedInstanceState.getParcelableArrayList(LIST_STATE_KEY);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void retryConnectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.error_internet_connection_title);
        builder.setMessage(R.string.error_internet_connection_message);

        builder.setNegativeButton(R.string.error_internet_connection_cancel_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.setPositiveButton(R.string.error_internet_connection_retry_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                makeRepositoryRequest(actualPage);
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        Toast.makeText(MainActivity.this, R.string.error_internet_connection_toast, Toast.LENGTH_SHORT).show();
    }
}
