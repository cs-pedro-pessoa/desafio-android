package br.com.githubjavapop.adapter;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.githubjavapop.R;
import br.com.githubjavapop.model.PullRequest;
import br.com.githubjavapop.model.User;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestHolder> {

    private List<PullRequest> pullRequests;

    public PullRequestAdapter(List<PullRequest> pullRequest) {
        this.pullRequests = pullRequest;
    }

    @Override
    public PullRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView
                = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pullrequest_list_item, parent, false);
        return new PullRequestHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(PullRequestHolder holder, int position) {
        PullRequest pullRequest = pullRequests.get(position);
        holder.bindPullRequest(pullRequest);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    static class PullRequestHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pullrequest_name)
        TextView pullRequestName;

        @BindView(R.id.pullrequest_description)
        TextView pullRequestDescription;

        @BindView(R.id.pullrequest_photo)
        ImageView pullRequestPhoto;

        @BindView(R.id.pullrequest_username)
        TextView pullRequestUserName;

        public PullRequestHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindPullRequest(PullRequest pullRequest) {
            User user = pullRequest.getUser();
            pullRequestName.setText(pullRequest.getPullRequestName());
            pullRequestDescription.setText(pullRequest.getPullRequestDescription());
            pullRequestUserName.setText(user.getLogin());
            Picasso.with(pullRequestPhoto.getContext()).load(user.getAvatarUrl()).into(pullRequestPhoto);
        }
    }

    public void addMorePullRequests(List<PullRequest> pullRequests) {
        this.pullRequests.addAll(pullRequests);
        notifyDataSetChanged();
    }


}
