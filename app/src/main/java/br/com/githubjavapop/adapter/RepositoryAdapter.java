package br.com.githubjavapop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.githubjavapop.R;
import br.com.githubjavapop.activity.PullRequestActivity;
import br.com.githubjavapop.model.Owner;
import br.com.githubjavapop.model.PullRequest;
import br.com.githubjavapop.model.Repository;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.attr.start;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryHolder> {

    public static final String REPO_OWNER = "OWNER";
    public static final String REPO_NAME = "NAME";

    private static List<Repository> repositories;

    public RepositoryAdapter(List<Repository> repositories) {
        this.repositories = repositories;
    }

    static class RepositoryHolder
            extends RecyclerView.ViewHolder implements View.OnClickListener {



        @BindView(R.id.repository_name)
        TextView name;

        @BindView(R.id.repository_description)
        TextView description;

        @BindView(R.id.repository_fork_icon)
        ImageView forksImage;

        @BindView(R.id.repository_fork_count)
        TextView forksCount;

        @BindView(R.id.repository_star_icon)
        ImageView starsImage;

        @BindView(R.id.repository_star_count)
        TextView starsCount;

        @BindView(R.id.repository_photo)
        ImageView thumbnail;

        @BindView(R.id.repository_username)
        TextView userName;

        @BindView(R.id.repository_fullname)
        TextView fullName;


        private Owner owner;

        public RepositoryHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            Intent intent = new Intent(context, PullRequestActivity.class);
            intent.putExtra(REPO_OWNER, owner.getLogin());
            intent.putExtra(REPO_NAME, name.getText().toString());
            context.startActivity(intent);
        }

        public void bindRepository(Repository repository) {
            this.owner = repository.getOwner();
            name.setText(repository.getName());
            description.setText(repository.getDescription());
            forksCount.setText(repository.getForksCount());
            starsCount.setText(repository.getStarsCount());
            userName.setText(owner.getLogin());
            Picasso.with(thumbnail.getContext()).load(owner.getAvatarUrl()).into(thumbnail);
        }
    }

    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView
                = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repository_list_item, parent, false);

        return new RepositoryHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position) {
        Repository repository = repositories.get(position);
        holder.bindRepository(repository);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public void addMoreRepositories(List<Repository> repositories) {
        this.repositories.addAll(repositories);
        notifyDataSetChanged();
    }


}
